const userList = [
    {
        id: 1,
        name: 'Kubuś Puchatek',
        author: 'Frank A.A. Milne'
    },
    {
        id: 2,
        name: 'Pinokio',
        author: 'Carlo Collodi'
    },
    {
        id: 3,
        name: 'Dzieci z Bullerbyn',
        author: 'Astrid Lindgren'
    }
];

export default userList;