import React, {useState} from 'react';

const AddUserForm = (props) => {

    const initUser = {id: null, name: '', author: ''};

    const [user, setUser] = useState(initUser);

    const handleChange = e => {
        const {name, value} = e.target;
        setUser({...user, [name]: value});
    }

    const handleSubmit = e => {
        e.preventDefault();
        if (user.name && user.author) {
            handleChange(e, props.addUser(user));
        }
    }

    return (
        <form>
            <label>Nazwa</label>
            <input className="u-full-width" type="text" value={user.name} name="name" onChange={handleChange} />
            <label>Autor</label>
            <input className="u-full-width" type="text" value={user.author} name="author" onChange={handleChange} />
            <button className="button-primary" type="submit" onClick={handleSubmit} >Dodaj</button>
        </form>
    )
}

export default AddUserForm;